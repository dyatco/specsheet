import React, { useState } from 'react';
import { Button, Form } from 'react-bootstrap';
import Products from "./Products";
import { Link, Redirect } from 'react-router-dom';
import { v4 as uuid } from "uuid";
import logo from '../assets/logo.png';
import profile from '../assets/profile.png';

export default function Add(){
	
	const[name, setName] = useState('');
	const[desc, setDesc] = useState('');

	const handleSubmit =(e) => {
		e.preventDefault();

		const ids = uuid();
		let uniqueId = ids.slice(0,8);
		let a = name;
		let b = desc;

		Products.push({id: uniqueId, Name : a, Desc : b});

		console.log(Products)
	}

	return (
		<div>

			<div class="container min-vw-100 px-0">
			  <nav>
			    <div class="d-flex">
			      <div class="mr-auto">
			        <Link class="navbar-brand w-100 pl-3 d-flex align-items-center h-100" to="/">
			          <img src={logo} alt="logo" class="logo"/>
			        </Link>
			      </div>
			      <div>
			        <a class="navbar-brand w-100 pr-3 d-flex align-items-center justify-content-end h-100" href="./pages/profile.html">
			          <img src={profile} alt="logo" class="profile"/>
			        </a>
			      </div>
			    </div>
			  </nav>

			  {/*Sidebar*/}
			  <div class="mx-0 px-0 d-flex justify-content-center">
				<div class="cafeSidebar">
					<div id="insideCafeSidebar">
						<div class="homeCafeSideText">
							<Link to="/" class="whiteText">Home</Link>
						</div>
						<div class="projectsCafeSideText">
							<Link to="/">Projects</Link>
						</div>
					</div>
				</div>
			</div>

			<Form className="d-grid gap-2" style={{margin:"15rem"}}>
				<Form.Group className="mb-3" controlId="formName">
					<Form.Control type="text" placeholder="Enter Name" required onChange={(e) => setName(e.target.value)}>
					</Form.Control>
				</Form.Group>
				<Form.Group className="mb-3" controlId="formDesc">
					<Form.Control type="text" placeholder="Enter Description" required onChange={(e) => setDesc(e.target.value)}>
					</Form.Control>
				</Form.Group>
				<Button onClick={(e) => handleSubmit(e)} type="submit" data-toggle="modal" data-target="#exampleModal">Submit</Button>
			</Form>
		

			<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	        <div class="modal-dialog">
	          <div class="modal-content text-dark">
	            <div class="modal-header">
	              <h5 class="modal-title text-dark" id="exampleModalLabel">Project added!</h5>
	              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                <span aria-hidden="true">&times;</span>
	              </button>
	            </div>
	            <div class="modal-footer">
	            <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
	          </div>
	        </div>
	      </div>
	      </div>
	      </div>
      </div>
)}

