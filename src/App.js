import { BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import './App.css';
import Home from './pages/Home';
import House from './pages/House';
import Cafe from './pages/Cafe';
import Apartment from './pages/Apartment';
import Add from './components/Add';

function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/" component={Home}/>
        <Route exact path="/create" component={Add}/>
        <Route exact path="/cafe" component={Cafe}/>
        <Route exact path="/house" component={House}/>
        <Route exact path="/apartment" component={Apartment}/>
      </Switch>
    </Router>
    
  );
}

export default App;
