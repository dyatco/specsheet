import { Link } from 'react-router-dom';
import logo from '../assets/logo.png';
import profile from '../assets/profile.png';
import cafeChair from '../assets/cafe/cafeChair.png';
import cafePlant from '../assets/cafe/cafePlant.png';
import cafeTable from '../assets/cafe/cafeTable.png';

export default function Cafe(){

	return(
	<div class="cafeContainer container-fluid">

	{/*Nav*/}
	<nav>
		<div class="d-flex">
			<div class="mr-auto">
				<Link class="navbar-brand w-100 pl-3 d-flex align-items-center h-100" to="/">
					<img src={logo} alt="logo" class="logo"/>
				</Link>
			</div>
			<div>
				<a class="navbar-brand w-100 pr-3 d-flex align-items-center justify-content-end h-100" href="./profile.html">
					<img src={profile} alt="profile" class="profile"/>
				</a>
			</div>
		</div>
	</nav>
	
	{/*Sidebar*/}
	<div class="mx-0 px-0 d-flex justify-content-center">
		<div class="cafeSidebar">
			<div id="insideCafeSidebar">
				<div class="homeCafeSideText">
					<Link to="/" class="whiteText">Home</Link>
				</div>
				<div class="projectsCafeSideText">
					<Link to="/">Projects</Link>
				</div>
			</div>
		</div>
	</div>

	{/*Table*/}
	<div class="col mt-3">
		<h5 class="projectHead">CAFE - 2020 Sta Rosa City</h5>
		<div class="table-responsive">
			<table class="table">
			  <thead class="thead">
			    <tr id="tableHead">
			      <th scope="col" class="w-25">Image</th>
			      <th scope="col">Name</th>
			      <th scope="col">Source</th>
			      <th scope="col">Price</th>
			    </tr>
			    <tr id="tableHeadMobile" class="w-100">
			      <th scope="col" >Image</th>
			      <th scope="col" >Details</th>
			    </tr>
			  </thead>
			  <tbody id="tableBody">
			    <tr>
			      <th scope="row" class="topLeft">
			      	<img src={cafeChair} class="w-100"/>
			      </th>
				      <td id="firstData">RÖNNINGE</td>
				      <td>IKEA</td>
				      <td>Php 5,950</td>
			    </tr>
			    <tr>
			      <th scope="row">
			      	<img src={cafeTable} class="w-100"/>
			      </th>
			      <td>STENSELE</td>
			      <td>IKEA</td>
			      <td>Php 6,990</td>
			    </tr>
			    <tr id="lastTableItem">
			      <th scope="row">
			      	<img src={cafePlant} class="w-100"/>
			      </th>
			      <td>Suamei</td>
			      <td>shopleaf.ph</td>
			      <td>Php 1,500</td>
			    </tr>
			  </tbody>
			</table>
		</div>
	</div>
</div>

)}