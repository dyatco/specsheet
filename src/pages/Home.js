import { Link } from 'react-router-dom';
import { Button } from 'react-bootstrap';
import apartment from '../assets/apartment.png';
import cafe from '../assets/cafe2.png';
import home from '../assets/home.png';
import logo from '../assets/logo.png';
import profile from '../assets/profile.png';
import Products from '../components/Products';

export default function Home(){

return(
	<div class="container min-vw-100 px-0">
	  <nav>
	    <div class="d-flex">
	      <div class="mr-auto">
	        <Link class="navbar-brand w-100 pl-3 d-flex align-items-center h-100" to="/">
	          <img src={logo} alt="logo" class="logo"/>
	        </Link>
	      </div>
	      <div>
	        <a class="navbar-brand w-100 pr-3 d-flex align-items-center justify-content-end h-100" href="./pages/profile.html">
	          <img src={profile} alt="logo" class="profile"/>
	        </a>
	      </div>
	    </div>
	  </nav>

	  {/*Sidebar*/}
	  <div class="sidebarContainer">
	    <div id="sidebar">
	      <div id="insideSidebar">
	        <div class="homeSideText"><Link to="/" class="whiteText">Home</Link>
	        </div>
	        <div class="projectsSideText "><Link to="/">Projects</Link>
	        </div>
	        <ul>
	          <li><Link to="/cafe">
	          Cafe</Link></li>
	          <li><Link to="/house">Filipino Home</Link></li>
	          <li><Link to="/apartment">Studio Apartment</Link></li>
	        </ul>
	      </div>
	      <div class="pb-5" id="bottomSidebar">PREMIUM</div>
	    </div>
	  </div>
	  
	  <div class="homeContainer">
		<main class="container">
		    <div class="homeText">HOME</div>
		    <div class="projectText d-flex justify-content-between pb-3">PROJECTS
		    	<Link to="/create">
			      <Button className="me-3">Add</Button>
			    </Link>
			</div>
		    
		    <div class="projects d-flex">
			    <div class="project col-4">
			      <Link to="/cafe">
			      <img src={cafe} alt="cafe" class="photo container"/>
			      <div class="projectText">CAFE</div>
			      <p>2020 Sta Rosa City</p>
			      </Link>
			    </div>
			    <div class="project col-4">
			      <Link to="/house">
			      <img src={home} alt="home" class="photo container"/>
			      <div class="projectText">FILIPINO HOME</div>
			      <p>2021 Muntinlupa City</p>
			      </Link>
			    </div>
			    <div class="project col-4">
			      <Link to="/apartment">
			      <img src={apartment} alt="apartment" class="photo container"/>
			      <div class="projectText">APARTMENT</div>
			      <p>2022 BGC Taguig City</p>
			      </Link>
			    </div>
			    { 
			    	Products && Products.length > 0
			    	?
			    	Products.map((item) =>{
			    		return(
			    			<div class="project col-4">
			    			  <Link to="/apartment">
			    			  <img src={apartment} alt="apartment" class="photo container-fluid"/>
			    			  <div class="projectText">{item.Name}</div>
			    			  <p>{item.Details}</p>
			    			  </Link>
			    			</div>
			    		)
			    	})
			    	:
			    	"No data available"
				}
		    </div>
		</main>
	</div>
	  
	</div>
)}

