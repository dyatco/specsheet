import { Link } from 'react-router-dom';
import logo from '../assets/logo.png';
import profile from '../assets/profile.png';
import bunkeflo from '../assets/apartment/bunkeflo.png';
import burvik from '../assets/apartment/burvik.png';
import klynnon from '../assets/apartment/klynnon.png';
import tufjord from '../assets/apartment/tufjord.png';

export default function Apartment(){
	return(

<div class="cafeContainer container-fluid">
	{/*Nav*/}
	<nav>
		<div class="d-flex">
			<div class="mr-auto">
				<Link class="navbar-brand w-100 pl-3 d-flex align-items-center h-100" to="/">
					<img src={logo} alt="logo" class="logo "/>
				</Link>
			</div>
			<div>
				<a class="navbar-brand w-100 pr-3 d-flex align-items-center justify-content-end h-100" href="./profile.html">
					<img src={profile} alt="profile" class="profile"/>
				</a>

			</div>
		</div>
	</nav>
	
	{/*Sidebar*/}
		<div class="mx-0 px-0 sidebarContainer d-flex justify-content-center">
			<div class="cafeSidebar">
				<div id="insideCafeSidebar">
					<div class="homeCafeSideText">
						<Link to="/" class="whiteText">Home</Link>
					</div>
					<div class="projectsCafeSideText">
						<Link to="/">Projects</Link>
					</div>
				</div>
			</div>
		</div>

	{/*Table*/}	
	<div class="col mt-3">
		<h5 class="projectHead">STUDIO APARTMENT - 2022 BGC</h5>
		<div class="table-responsive">
			<table class="table">
			  <thead class="thead">
			    <tr id="tableHead">
			      <th scope="col" class="w-25">Image</th>
			      <th scope="col">Name</th>
			      <th scope="col">Source</th>
			      <th scope="col">Price</th>
			    </tr>
			    <tr id="tableHeadMobile" class="w-100">
			      <th scope="col" >Image</th>
			      <th scope="col" >Details</th>
			    </tr>
			  </thead>
			  <tbody id="tableBody">
			    <tr>
			      <th scope="row" class="topLeft">
			      	<img src={bunkeflo} class="w-100"/>
			      </th>
				      <td id="firstData">BUNKEFLO</td>
				      <td>IKEA</td>
				      <td>Php 1,890</td>
			    </tr>
			    <tr>
			      <th scope="row">
			      	<img src={klynnon} class="w-100"/>
			      </th>
			      <td>KLYNNON</td>
			      <td>IKEA</td>
			      <td>Php 1,590</td>
			    </tr>
			    <tr>
			      <th scope="row">
			      	<img src={tufjord} class="w-100"/>
			      </th>
			      <td>TUFJORD</td>
			      <td>IKEA</td>
			      <td>Php 37,990</td>
			    </tr>
			    <tr id="lastTableItem">
			      <th scope="row">
			      	<img src={burvik} class="w-100"/>
			      </th>
			      <td>BURVIK</td>
			      <td>IKEA</td>
			      <td>Php 1,990</td>
			    </tr>
			  </tbody>
			</table>
		</div>
	</div>

</div>
	)
}