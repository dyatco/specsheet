import { Link } from 'react-router-dom';
import logo from '../assets/logo.png';
import profile from '../assets/profile.png';
import coffeeTable from '../assets/home/coffeeTable.png';
import loungeChair from '../assets/home/loungeChair.png';
import sofa from '../assets/home/sofa.png';

export default function House(){

	return(
	<div class="cafeContainer container-fluid">
	{/*Nav*/}
	<nav>
		<div class="d-flex">
			<div class="mr-auto">
				<Link class="navbar-brand w-100 pl-3 d-flex align-items-center h-100" to="/">
					<img src={logo} alt="logo" class="logo "/>
				</Link>
			</div>
			<div>
				<Link class="navbar-brand w-100 pr-3 d-flex align-items-center justify-content-end h-100" to="/">
					<img src={profile} alt="profile" class="profile"/>
				</Link>
			</div>
		</div>
	</nav>

	{/*Sidebar*/}
		<div class="mx-0 px-0 sidebarContainer d-flex justify-content-center">
			<div class="cafeSidebar">
				<div id="insideCafeSidebar">
					<div class="homeCafeSideText">
						<Link to="/" class="whiteText">Home</Link>
					</div>
					<div class="projectsCafeSideText">
						<Link to="/">Projects</Link>
					</div>
				</div>
			</div>
		</div>

	{/*Table*/}
		<div class="col mt-3">
		<h5 class="projectHead">FILIPINO HOME - 2021 Muntinlupa City</h5>
		<div class="table-responsive">
			<table class="table">
			  <thead class="thead">
			    <tr id="tableHead">
			      <th scope="col" class="w-25">Image</th>
			      <th scope="col">Name</th>
			      <th scope="col">Source</th>
			      <th scope="col">Price</th>
			    </tr>
			    <tr id="tableHeadMobile" class="w-100">
			      <th scope="col" >Image</th>
			      <th scope="col" >Details</th>
			     </tr>
			  </thead>
			  <tbody id="tableBody">
			    <tr>
			      <th scope="row" class="topLeft">
			      	<img src={coffeeTable} class="w-100"/>
			      </th>
				      <td id="firstData">GAMLEHULT</td>
				      <td>IKEA</td>
				      <td>Php 4,990</td>
			    </tr>
			    <tr>
			      <th scope="row">
			      	<img src={loungeChair} class="w-100"/>
			      </th>
			      <td>BUSKBO</td>
			      <td>IKEA</td>
			      <td>Php 7,280</td>
			    </tr>
			    <tr id="lastTableItem">
			      <th scope="row">
			      	<img src={sofa} class="w-100"/>
			      </th>
			      <td>SLATORP</td>
			      <td>IKEA</td>
			      <td>Php 59,990</td>
			    </tr>
			  </tbody>
			</table>
		</div>
	</div>
</div>
)}